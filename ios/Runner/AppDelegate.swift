import UIKit
import Flutter
import os.log

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
        ) -> Bool {
        
        UIDevice.current.isBatteryMonitoringEnabled = true
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(batteryStateChanged),
            name: .UIDeviceBatteryStateDidChange,
            object: nil)
        _ = Timer.scheduledTimer(timeInterval: 900.0, target: self, selector: Selector(("sayHello")), userInfo: nil, repeats: true)

        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    @objc func sayHello()
    {
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        
        let channel = FlutterBasicMessageChannel(
            name: "foo",
            binaryMessenger: controller,
            codec: FlutterStringCodec.sharedInstance())
        
        let device = UIDevice.current
        device.isBatteryMonitoringEnabled = true
        
        var battery = "0"
        
        if device.batteryState == UIDeviceBatteryState.unknown {
            battery = "unavailable"
        } else {
            battery = String(device.batteryLevel * 100)
        }
        
        channel.sendMessage("Time_Check;" + battery + "%") {(reply: Any?) -> Void in
            if #available(iOS 10.0, *) {
                os_log("%@", type: .info, reply as! String)
            } else {
                // Fallback on earlier versions
            }
            
        }
    }
    
    var chargingVar = "" //Empty Variable
    
    @objc func batteryStateChanged(){
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        
        let channel = FlutterBasicMessageChannel(
            name: "foo",
            binaryMessenger: controller,
            codec: FlutterStringCodec.sharedInstance())
        // Send message to Dart and receive reply.

        // Receive messages from Dart and send replies.
        channel.setMessageHandler {
            (message: Any?, reply: FlutterReply) -> Void in
            if #available(iOS 10.0, *) {
                os_log("Received: %@", type: .info, message as! String)
            } else {
                // Fallback on earlier versions
            }
            reply("Hi from iOS")
        }
        
        let device = UIDevice.current
        device.isBatteryMonitoringEnabled = true
        
        var battery = "0"
        
        if device.batteryState == UIDeviceBatteryState.unknown {
            battery = "unavailable"
        } else {
            battery = String(device.batteryLevel * 100)
        }
        
        if (UIDevice.current.batteryState == .charging) {    //Here we check if the device is charging
            UIApplication.shared.isIdleTimerDisabled = true  //Here we disable the lock screen time out value
            self.chargingVar = "charging" //Here we change the variable to "is charging" 😀
            //If power is pluged in we send an Alert
            channel.sendMessage("charging;" + battery + "%") {(reply: Any?) -> Void in
                if #available(iOS 10.0, *) {
                    os_log("%@", type: .info, reply as! String)
                } else {
                    // Fallback on earlier versions
                }
            }
        }else{
            self.chargingVar = "not charging"  //Here we change the variable to "is not charging"  😥
            channel.sendMessage("not charging;" + battery + "%") {(reply: Any?) -> Void in
                if #available(iOS 10.0, *) {
                    os_log("%@", type: .info, reply as! String)
                } else {
                    // Fallback on earlier versions
                }
            }
            //If power is not pluged we send an Alert
        }
        
        
    }
}
