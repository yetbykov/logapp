import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class MapUtils {
  static openMap(double latitude, double longitude) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }
}

void main() {
  runApp(
    MaterialApp(
      title: 'Reading and Writing Files',
      home: FlutterDemo(storage: LogStorage()),
    ),
  );
}

class LogStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/log.txt');
  }

  Future<String> readFile() async {
    try {
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();

      return contents;
    } catch (e) {
      // If encountering an error, return 0
      return "";
    }
  }

  Future<File> writeFile(String txt) async {
    final file = await _localFile;

    // Write the file
    return file.writeAsString('$txt');
  }
}

class FlutterDemo extends StatefulWidget {
  final LogStorage storage;

  FlutterDemo({Key key, @required this.storage}) : super(key: key);

  @override
  _FlutterDemoState createState() => _FlutterDemoState();
}

class _FlutterDemoState extends State<FlutterDemo> {
  String _log;
  static var beforeDate = DateTime.now().toUtc();
  var date =
      new DateTime.utc(beforeDate.year, beforeDate.month, beforeDate.day);
  var formatter = new DateFormat('yyyy-MM-dd');

  void getLoc() async {
    Geolocator geolocator = Geolocator();

    Position userLocation;
    userLocation = await geolocator.getCurrentPosition(
                      desiredAccuracy: LocationAccuracy.best);
  }

  @override
  void initState() {
    super.initState();
    
    getLoc();
    
    widget.storage.readFile().then((String value) {
      setState(() {
        _log = value;
      });
    });

    const channel = BasicMessageChannel<String>('foo', StringCodec());

    // Send message to platform and receive reply.
    channel.send('Hello, world').then((onValue) {
      print(onValue);
    });

    // Receive messages from platform and send replies.
    channel.setMessageHandler((String message) async {
      print('Received: $message');
      if (message == "LOG_UPDATE") {
        widget.storage.readFile().then((String value) {
          setState(() {
            _log = value;
          });
        });
      } else {
        var now = new DateTime.now();
        Geolocator geolocator = Geolocator();

        Position userLocation;
        userLocation = await geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.best);

        setState(() {
          _log += "$now;$message;" +
              userLocation.latitude.toString() +
              "," +
              userLocation.longitude.toString() +
              "\n";
          widget.storage.writeFile(_log);
        });
      }
      return 'Hi from Dart';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Log')),
      body: SingleChildScrollView(
          child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              FlatButton(
                child: Text("<"),
                onPressed: () {
                  setState(() {
                    date = date.subtract(new Duration(days: 1));
                  });
                },
              ),
              Text(formatter.format(date)),
              FlatButton(
                child: Text(">"),
                onPressed: () {
                  setState(() {
                    date = date.add(new Duration(days: 1));
                  });
                },
              )
            ],
          ),
          Table(
            border: TableBorder.all(width: 1.0, color: Colors.black),
            children: [
              if (_log != null)
                for (var item in _log.split('\n'))
                  if (item.split(';').length >= 4 &&
                      DateTime.utc(
                                  DateTime.parse(item.split(';')[0])
                                      .toUtc()
                                      .year,
                                  DateTime.parse(item.split(';')[0])
                                      .toUtc()
                                      .month,
                                  DateTime.parse(item.split(';')[0])
                                      .toUtc()
                                      .day)
                              .compareTo(date) ==
                          0)
                    TableRow(
                      children: [
                        if (item.split(';').length >= 4)
                          TableCell(child: Text(item.split(';')[0])),
                        if (item.split(';').length >= 4)
                          TableCell(child: Text(item.split(';')[1])),
                        if (item.split(';').length >= 4)
                          TableCell(child: Text(item.split(';')[2])),
                        if (item.split(';').length >= 4)
                          TableCell(
                              child: FlatButton(
                                  child: Text(item.split(';')[3]),
                                  onPressed: () {
                                    MapUtils.openMap(
                                        double.parse(
                                            item.split(';')[3].split(',')[0]),
                                        double.parse(
                                            item.split(';')[3].split(',')[1]));
                                  })),
                        if (item.split(';').length < 4)
                          TableCell(child: Text("")),
                        if (item.split(';').length < 4)
                          TableCell(child: Text("")),
                        if (item.split(';').length < 4)
                          TableCell(child: Text("")),
                        if (item.split(';').length < 4)
                          TableCell(child: Text("")),
                      ],
                    ),
            ],
          )
        ],
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _log = "";
          });
          widget.storage.writeFile("");
        },
        tooltip: 'Erase',
        child: Icon(Icons.remove_circle),
      ),
    );
  }
}
