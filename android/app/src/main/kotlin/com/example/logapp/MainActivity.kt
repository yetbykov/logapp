package com.example.logapp

import android.annotation.SuppressLint
import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_POWER_CONNECTED
import android.content.Intent.ACTION_POWER_DISCONNECTED
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Location
import android.os.BatteryManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.BasicMessageChannel
import io.flutter.plugin.common.StringCodec
import io.flutter.plugins.GeneratedPluginRegistrant
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter
import java.text.SimpleDateFormat
import java.util.*


class MainActivity: FlutterActivity() {
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private val timeCheckInterval = 1000 * 60 * 15 //15 minutes
    var timeCheckHandler = Handler()
    val timeCheckHandlerTask = object : Runnable {
        @RequiresApi(VERSION_CODES.LOLLIPOP)
        @SuppressLint("MissingPermission") //we assume that Flutter app has requested permissions already
        override fun run() {
            fusedLocationClient.lastLocation
                    .addOnSuccessListener { location : Location? ->
                        if (location != null) {
                            var currentDateAndTime = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date())
                            val batteryManager = getSystemService(Context.BATTERY_SERVICE) as BatteryManager
                            val batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
                            var location = location.latitude.toString() + "," + location.longitude.toString()

                            val entryString = "$currentDateAndTime;Time_Check;$batteryLevel;$location\n"
                            val fOut = FileOutputStream ( File(applicationInfo.dataDir + "/app_flutter/log.txt"), true)
                            val osw = OutputStreamWriter(fOut)
                            osw.write(entryString)
                            osw.flush()
                            osw.close()

                            val channel = BasicMessageChannel<String>(
                                    getInstance()?.flutterView, //we get flutter view from MainActivity because on the time of this process flutterView could be changed already
                                    "foo", StringCodec.INSTANCE)

                            //Notify flutter about our log entry (if app is open, this message will be used to redraw UI)
                            channel.send("LOG_UPDATE") {}
                        }
                    }
                    .addOnFailureListener {
                        var currentDateAndTime = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date())
                        val batteryManager = getSystemService(Context.BATTERY_SERVICE) as BatteryManager
                        val batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
                        var location = "NO,PERMISSION"

                        val entryString = "$currentDateAndTime;Time_Check;$batteryLevel;$location\n"
                        val fOut = FileOutputStream ( File(applicationInfo.dataDir + "/app_flutter/log.txt"), true)
                        val osw = OutputStreamWriter(fOut)
                        osw.write(entryString)
                        osw.flush()
                        osw.close()

                        val channel = BasicMessageChannel<String>(
                                getInstance()?.flutterView, //we get flutter view from MainActivity because on the time of this process flutterView could be changed already
                                "foo", StringCodec.INSTANCE)

                        //Notify flutter about our log entry (if app is open, this message will be used to redraw UI)
                        channel.send("LOG_UPDATE") {}
                    }
                    .addOnCanceledListener {
                        var currentDateAndTime = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date())
                        val batteryManager = getSystemService(Context.BATTERY_SERVICE) as BatteryManager
                        val batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
                        var location = "NOT,AVAIL"

                        val entryString = "$currentDateAndTime;Time_Check;$batteryLevel;$location\n"
                        val fOut = FileOutputStream ( File(applicationInfo.dataDir + "/app_flutter/log.txt"), true)
                        val osw = OutputStreamWriter(fOut)
                        osw.write(entryString)
                        osw.flush()
                        osw.close()

                        val channel = BasicMessageChannel<String>(
                                getInstance()?.flutterView, //we get flutter view from MainActivity because on the time of this process flutterView could be changed already
                                "foo", StringCodec.INSTANCE)

                        //Notify flutter about our log entry (if app is open, this message will be used to redraw UI)
                        channel.send("LOG_UPDATE") {}
                    }
            timeCheckHandler.postDelayed(this, timeCheckInterval.toLong())
        }
    }

    //here we initialize MainActivity getter for instance, so we can use methods and fields from it in other classes
    companion object {
        var ins: MainActivity? = null
        fun getInstance(): MainActivity? {
            return ins
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
      super.onCreate(savedInstanceState)
      ins = this
      fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
      GeneratedPluginRegistrant.registerWith(this)
    }

    override fun onStart() {
        super.onStart()
        var startIntent = Intent(applicationContext, BackgroundService::class.java)
        startIntent.action = "ACTION_START_SERVICE"
        startService(startIntent)
    }

    @SuppressLint("MissingPermission")
    @RequiresApi(VERSION_CODES.LOLLIPOP)
    fun logPowerPlugMessage(isPluggedIn: Boolean) {
        fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    if (location != null) {
                        var currentDateAndTime = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date())
                        val batteryManager = getSystemService(Context.BATTERY_SERVICE) as BatteryManager
                        val batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
                        var location = location.latitude.toString() + "," + location.longitude.toString()

                        var msg = if (isPluggedIn) {
                            "charging"
                        } else {
                            "not charging"
                        }

                        val entryString = "$currentDateAndTime;$msg;$batteryLevel;$location\n"
                        val fOut = FileOutputStream ( File(applicationInfo.dataDir + "/app_flutter/log.txt"), true)
                        val osw = OutputStreamWriter(fOut)
                        osw.write(entryString)
                        osw.flush()
                        osw.close()

                        val channel = BasicMessageChannel<String>(flutterView, "foo", StringCodec.INSTANCE)

                        //Notify flutter about our log entry (if app is open, this message will be used to redraw UI)
                        channel.send("LOG_UPDATE") {}
                    }
                }
    }

    class BackgroundService : Service() {
        private var powerReceiver: BroadcastReceiver? = null
        private val notificationId = 543
        var isServiceRunning = false

        override fun onCreate() {
            super.onCreate()
            powerReceiver = PowerConnectionReceiver()
            startServiceWithNotification()
        }

        override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
            if (intent != null && intent!!.action == "ACTION_START_SERVICE") {
                startServiceWithNotification()
            } else
                stopMyService()
            return START_STICKY
        }

        // In case the service is deleted or crashes
        override fun onDestroy() {
            isServiceRunning = false
            super.onDestroy()
        }

        override fun onBind(intent: Intent): IBinder? {
            // Used only in case of bound services.
            return null
        }

        private fun startServiceWithNotification() {
            if (isServiceRunning) return
            isServiceRunning = true

            val notificationIntent = Intent(applicationContext, MainActivity::class.java)
            notificationIntent.action = "ACTION_MAIN"
            notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            val contentPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

            val icon = BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher)

            var channelId = "com.example.logapp"

            //channelId is only used in O and probably any later Android version
            if (VERSION.SDK_INT >= VERSION_CODES.O) {
                var channelName = "Log App Background Service"
                var chan = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_NONE)
                chan.lightColor = Color.BLUE
                chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE;
                var manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                manager.createNotificationChannel(chan)
            } else {
                channelId = ""
            }

            val notification = NotificationCompat.Builder(this, channelId)
                    .setContentTitle("Log app")
                    .setTicker("Log app")
                    .setContentText("Logging in background...")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                    .setContentIntent(contentPendingIntent)
                    .setOngoing(true)
                    .build()
            notification.flags = notification.flags or Notification.FLAG_NO_CLEAR // NO_CLEAR makes the notification stay when the user performs a "delete all" command
            startForeground(notificationId, notification)

            //Register our power receiver for plug/unplug actions
            registerReceiver(powerReceiver, IntentFilter(ACTION_POWER_CONNECTED))
            registerReceiver(powerReceiver, IntentFilter(ACTION_POWER_DISCONNECTED))

            //Run our Time Check task
            getInstance()?.timeCheckHandlerTask?.run()
        }

        private fun stopMyService() {
            stopForeground(true)
            stopSelf()
            isServiceRunning = false

            //Unregister all actions from receiver
            unregisterReceiver(powerReceiver)

            //Cancel Time Check task
            getInstance()?.timeCheckHandler?.removeCallbacks(getInstance()?.timeCheckHandlerTask)
        }
    }

    class PowerConnectionReceiver : BroadcastReceiver() {
        @RequiresApi(VERSION_CODES.LOLLIPOP)
        override fun onReceive(context: Context, intent: Intent) {
            getInstance()?.logPowerPlugMessage(isPlugged(context))
        }

        private fun isPlugged(context: Context): Boolean {
            var isPlugged: Boolean
            val intent = context.registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
            val plugged = intent!!.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)
            isPlugged = plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB
            if (VERSION.SDK_INT > VERSION_CODES.JELLY_BEAN) {
                isPlugged = isPlugged || plugged == BatteryManager.BATTERY_PLUGGED_WIRELESS
            }
            return isPlugged
        }
    }
}